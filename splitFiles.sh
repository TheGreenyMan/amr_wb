#!/bin/bash
# script to split large wave files.
#set -x
if [ "$1" == "-h" ]
then
echo "Loop through all the wav files and splits them into chunks. This need sox and soxi installed."
else
command -v soxi >/dev/null 2>&1 || { echo "This scrpit requires soxi but it's not installed.  Aborting. To install sudo apt-get install soxi" >&2; exit 1; }
command -v sox >/dev/null 2>&1 || { echo "This scrpit requires sox but it's not installed.  Aborting. To install sudo apt-get install sox" >&2; exit 1; }

FILE_EXT=wav
WAV_ROOT_DIR=wav
WAV_OUT_DIR=wav-splits
FILES=$(ls $WAV_ROOT_DIR)
MIN_DURATION=16 # seconds
for file in $FILES
	do
	if [[ $file == *".wav" ]]
		then
                flen=$(soxi -D $WAV_ROOT_DIR/$file)
		#change float to int
		flen=$(awk -v var=$flen 'BEGIN{split(var,a,".");print(a[1]) ; "flen=a[1]"; print(flen);}')	
		nSplits=$((flen / MIN_DURATION))
		nc=$(soxi -c $WAV_ROOT_DIR/$file)
		if [ "$nSplits" -ge 2 ] || [ "$nc" -ge 2 ]
			then
			echo "slpitting $file into $nSplits files."
			sleep 0.25
			clear
			for i in $(seq 1 $nSplits) ; do
				fileDry=$(sed 's/\.wav//g'<<<"cln"$file)_$i
				start=$(((i-1)*MIN_DURATION))
				sox $WAV_ROOT_DIR/$file $WAV_OUT_DIR/$fileDry.$FILE_EXT trim $start $MIN_DURATION
		
			done
		else
                echo "skipping... $file size too small or not mono"
		fi
	else 
		echo "skipping... $file not a wav"
	fi	
done
echo "removing empty files"
sleep 2
FILES=$(ls $WAV_OUT_DIR)
for file in $FILES
	do	
	flen=$(soxi -D $WAV_OUT_DIR/$file)
	#change float to int
	flen=$(awk -v var=$flen 'BEGIN{split(var,a,".");print(a[1]) ; "flen=a[1]"; print(flen);}')
	if [ "$flen" -le 0 ]
	then
	echo "removing $file"
	rm $WAV_OUT_DIR/$file
	fi
done
fi
