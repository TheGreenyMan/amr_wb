#!/bin/bash
# hack we know that the files are 30 sec_Long
#set -x
if [ "$1" == "-h" ]
then
echo "Loop through all the wav files and mixes them with one of the two noise segments choosen randomly."
else
FILE_EXT=wav
NOISE_DIR=backgroundSound
NOISE_LEN=30
NOISE_1=noise.wav
NOISE_1H="wns"
NOISE_2=tone.wav
NOISE_2H="ton"
WAV_DIR=wav-splits
MIX_FILE=mix
FILES=$(ls $WAV_DIR)

for file in $FILES	
	do
	# get the duration from the splitted files convert to int in secs
	MIN_DURATION=$(soxi -D $WAV_DIR/$file)
	MIN_DURATION=$(awk -v var=$MIN_DURATION 'BEGIN{split(var,a,".");print(a[1]) ; "flen=a[1]"; print(flen);}')	
	if [[ $file == *".wav" ]]
	then
	x=$(( ( RANDOM % 2 )  + 0 ))
	if [[ $x == 1 ]]
	then # choose first file
		noiseFile=$NOISE_DIR/$NOISE_1
		noiseH=$NOISE_1H
	else # choose second file
		noiseFile=$NOISE_DIR/$NOISE_2
		noiseH=$NOISE_2H
	fi
	# split randomly the file to MIN_DURATION
	nSplits=$((NOISE_LEN / MIN_DURATION))
	start=$(( RANDOM % $nSplits * MIN_DURATION )) 
	sox $noiseFile $NOISE_DIR/1$MIX_FILE.$FILE_EXT trim $start $MIN_DURATION
	sox -v 0.02 $NOISE_DIR/1$MIX_FILE.$FILE_EXT $NOISE_DIR/$MIX_FILE.$FILE_EXT
	#now mix the files
        sox -m $WAV_DIR/$file  $NOISE_DIR/$MIX_FILE.$FILE_EXT  $WAV_DIR/$noiseH$file
	#soxi -D $noiseFile
	echo "mixing $WAV_DIR/$file and $noiseFile"	
	fi
done
fi
