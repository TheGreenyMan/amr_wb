#!/bin/bash
if [ "$1" == "-h" ]
then
echo "Loop through all the wav files and encodes them using amr-wb."
else
WAV_ROOT_DIR=wav-splits
FILES=$(ls $WAV_ROOT_DIR)
for file in $FILES
	do
	if [[ $file == *".wav" ]]
		then
		echo "processing $file"
		./encodeSingleFile.sh $WAV_ROOT_DIR/$file
	else 
		echo "skipping... $file"
	fi	
done
echo "shaping data..."
sed -r 's/,/\t/g;s/.wav/\t/g;s/wav-splits\///g;s/\t//g' stats.txt  > lpcstats.txt
rm stats.txt
n=$(wc -l lpcstats.txt)
m=($n)
sed -i '1 i\'$m'' lpcstats.txt
fi
