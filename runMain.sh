#!/bin/bash
ctr=0
nruns=$1
RUN_FILE=runs.txt
if [[ "$1" -le 0 ]]
then 
	echo "Give a stricktly positive number of runs."
else
if [[ -f "$RUN_FILE" ]]
then rm $RUN_FILE
fi
while [[ $ctr -le $1 ]]; do
	echo "processing run nbr $ctr. $(( nruns-ctr )) runs to be done." 
	echo $ctr
	python main/main.py -i lpcstats.txt -s 3 > stats.data
	ctr=$(( ctr+1 ))
	tail -1 stats.data >> $RUN_FILE 
done
fi
