'''
Created on 2015-09-20

@author: kintar
'''

class utils(object):
    def __init__(self):
        pass
    'QuickSearch'
    def isInQuick(self, val, low, high,  arr):
        piv = int((high-low)/2) + low;
        if low>high:
            return False
        if arr[piv]==val:
            return True
        if (val < arr[piv]):
            return self.isInQuick(val,low, piv-1,arr)
        if (val > arr[piv]):
            return self.isInQuick(val,piv+1, high, arr)
        