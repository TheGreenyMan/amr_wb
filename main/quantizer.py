'''
Created on 2015-09-20

@author: kintar
'''

'Main class to contain methods to manage qvs.'
from numpy import Inf
'A qv is defined as a collection of vectors.'
from vector import vector
import numpy
class quantizer:
    def __init__(self,tag, nElement,dimElement):
        self.tag=tag[0:vector.tagLen]
        self.nChildren=[0 for i in xrange(nElement)];
        self.nElement=nElement
        self.dimElement=dimElement
        self.data=[vector(dimElement, tag[0:2] + str(i)) for i in range(nElement)]
        self.leader=[self.tag[0:2]+str(i) for i in range(nElement)];
        self.leaderCnt=[[0.0 for i in range(nElement)]for i in range(nElement)];
        self.initialised=[0 for i in range(self.nElement)];
        self.glbIndex=0;
    'return an item given an index'
    def __getitem__(self,index):
        return self.data[index] 
    'sets an item given an index'
    def __setitem__(self,index, value):
        self.data[index]=value
    ' supposes the leader are distinct based on their tags'
    def getLeaderIndex(self, leaderTag):
        for i in range(self.nElement):
            if (self.leader[i]==leaderTag):
                return i
        return  -1
    'loops the data looking for a unique set of tags'
    def initiate(self, values):
        if (values.dim==self.dimElement):            
            leader = self.getLeaderIndex(values.tag)
            if (leader==-1):
                uidx = self.glbIndex % self.nElement
                self.leader[uidx]=values.tag;                
                self.glbIndex=self.glbIndex+1;
        else:
            raise ValueError("set size mismatch", values.dim, self.dimElement)      
        return
    'updates the qv given and index i'          
    def update(self, index, values): 
        if (values.dim==self.dimElement):            
            self.nChildren[index]=self.nChildren[index]+1
            self.data[index].tag=values.tag;
            leader=self.getLeaderIndex(values.tag);
            self.leaderCnt[index][leader]=self.leaderCnt[index][leader]+1;
            n = float(self.nChildren[index])
            'updates the weights for each vector'
            for i in xrange(self.dimElement):
                self.data[index][i]=round(self.data[index][i]*(n-1)/n+values[i]*1/n,3)
        else:
            raise ValueError("set size mismatch", values.dim, self.dimElement)
    
    'loops the leaders cnt and chooses the leader'    
    def updateLeader(self):
        print "Updating leaders.."
        for i in range(self.nElement):
            maxIndex=self.leaderCnt[i].index(max(self.leaderCnt[i]))
            self.leader[i]=self.leader[maxIndex]
    
    def __str__(self):
        s=""
        for i in range(self.nElement):
            s = s+ repr(self.nChildren[i]) +":"+str(self.data[i])+"\n"
        s=s+"leader: " + repr(self.leader) + "\n" + "leaderCnt: " + repr(self.leaderCnt)
        return s;
    'returns the closest item'
    def findClosest(self,vector):
        d=+Inf;index=-1;
        for i in range(self.nElement):
            d_new = self.data[i].L2(vector);
            if (d_new<d):
                index=i;
                d=d_new;
        return index;
    'verifies if the VQ is consistent'
    def isConsistentQV(self):
        for i in range(self.nElement):
            for j in range(self.nElement):
                if (self[i].scorer(self.data[j])):
                    return False
        return True