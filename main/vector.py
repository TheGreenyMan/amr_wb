'''
Created on 2015-09-20

@author: kintar
'''

'vector class contains methods for manipulating vectors'
class vector:
    tagLen=3
    def __init__(self, dim, tag):
        self.tag = tag[0:vector.tagLen]
        self.dim=dim
        self.data=[0.0 for i in xrange(dim)]
    'get the item given an index'
    def __getitem__(self,index):
        return self.data[index]
    'set an item for a given index'
    def __setitem__(self,index, value):
        self.data[index]=value
    'sets values'    
    def setValues(self,data):
        for i in range (self.dim):
            self.data[i]=data[i]
    'L2 norm of a v1-v2'              
    def L2(self, vector):
        d=0.0
        if (vector.dim==self.dim):
            for i in xrange(self.dim):
                d=d+(self.data[i]-vector[i])**2
        else:
            raise ValueError("L2 size mismatch", self.dim, vector.dim)
        return d
    def __str__(self):
        str= self.tag + ":["
        for i in range(self.dim):
            str = str +" "+ repr(self.data[i])
        str = str + "]"
        return str;  
    'splits a string and gets the values and tag'
    def linetoVector(self, line):
        ctr=line.split(" ");
        self.tag=ctr[0];
        self.tag=self.tag[0:vector.tagLen];
        self.data=[float(ctr[x])/float(ctr[1]) for x in range(1,len(ctr)-1)]
        return self;
    'returns 1 if the first 3 char of tags match'
    def scorer(self, v2):
        if self.tag==v2.tag:
            return 1;
        else:
            return 0;