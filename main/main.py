'''
Created on 2015-09-17

@author: klakhdhar
naive classifier for some coeff from amr-wb+ to see if there is an'
'impact of the background noise on these coefficients.'
'''
from quantizer import quantizer
from vector import vector
from utils import utils
import random 
import os
import sys
import argparse
from PIL.GimpGradientFile import EPSILON
from oauthlib.oauth2.rfc6749 import catch_errors_and_unavailability
# file name
dataFile=''
# VQ specific variables
VQ_dim=10;        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This is main quantizer help.')
    parser.add_argument('-i','--input', help='Input file name',required=True)
    parser.add_argument('-s','--size',help='Quantizer size', required=True)
    args = parser.parse_args()
# get the values from the parser
dataFile=args.input
VQ_size=int(args.size)
# Definitions
u= utils()
#path = os.path.dirname(os.getcwd())
# data is formatted as below :
def usage():
    print ( "First line size of data\n"
    "each line = 'tag, [values]'\n"
    "for this specific case the data is lpc coeff (10 of them) collected from a popular speech coder for some speech segments:\n"
        "\t1- clean speech.\n"
        "\t2- speech same speaker (different words) with background noise 1 tone(ton).\n"
        "\t3- speech same speaker (different words) with background noise 2 white noise (wns).\n"
)
VQ = quantizer("dn8",VQ_size, VQ_dim)
# Randomly initiate the VQ !
# read data file
try:
    fdata=open(dataFile)
except:
    print "File "+  dataFile + " not found !"
    sys.exit(2)
usage()
nData=int(fdata.readline())
nTrnData=nData/100;
idxTrn=random.sample(range(int(nData)), int(nTrnData))
idxTrn=sorted(idxTrn)
index=0
line=fdata.readline();
v=vector(VQ_dim,"tmp");
'loop and init the VQ'
print "Exploring data..."
while (line):
        v=vector.linetoVector(v, line)
        VQ.initiate(v)
        line=fdata.readline();
fdata.close()
print(VQ)
fdata=open(dataFile)
line=fdata.readline()
#update the leaders of each vector in the VQ
print ("Training VQ using "+repr(nTrnData)+" vectors")
# Train the data set:
ctr=0
nctr=0;
line=fdata.readline();
while (line):
    if (ctr < nTrnData):
        if (u.isInQuick(nctr, 0, nTrnData-1, idxTrn)):
            v=vector(VQ_dim,"tmp");
            v=vector.linetoVector(v, line)
            #i = VQ.findClosest(v)
            i=VQ.getLeaderIndex(v.tag)
            VQ.update(i, v)
            ctr = ctr+1
        line=fdata.readline();
    else:
        print ("done")
        break;
    nctr=nctr+1

VQ.updateLeader()
#close the file 
fdata.close()
fdata=open(dataFile)
line=fdata.readline()
line=fdata.readline()
print VQ
# Now classify vectors:
stats=[EPSILON for i in range (VQ_size)]
score=[0.0 for i in range(VQ_size)]
ctr=0;
linectr=0;
while(line):
    #if (linectr not in idxTrn):
    if (not u.isInQuick(linectr, 0, nTrnData-1, idxTrn)):
        ctr = ctr+1
        prg=int(100*ctr/(nData-nTrnData))
        sys.stdout.write("\r%d%%" % prg)
        sys.stdout.flush()
        v=vector(VQ_dim,"tmp");
        v=vector.linetoVector(v, line)
        i = VQ.findClosest(v)
    #   update stats for each VQ
        stats[i]=stats[i]+1;
        score[i]=score[i]+v.scorer(VQ[i])
    line=fdata.readline()
    linectr=linectr+1

fstats=[stats[i]/(nData-nTrnData+EPSILON) for i in range(VQ_size)]
fscore=[score[i]/stats[i] for i in range(VQ_size)]
print " \nResult: "
print "stats: " 
print ["%0.2f" % i for i in fstats]
print "scores: " 
print  ["%0.2f" % i for i in fscore]
